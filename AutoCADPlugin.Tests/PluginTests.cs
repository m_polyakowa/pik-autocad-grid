using Xunit;
using AutoCADPluginWpf;
using Autodesk.AutoCAD.Geometry;


namespace AutoCADPlugin.Tests
{
    public class PluginTests
    {
        [Theory]
        [InlineData(new double[] { 0.26, 5.84, -2.34, 11.73 }, true)]
        [InlineData(new double[] { 0, 1, 1, 1 }, false)]
        [InlineData(new double[] { 0.74, 2.87, -15, 11.73 }, true)]
        public void RigthThreeTest(double[] coord, bool expected)
        {
            var rigth = Commands.RightThree(coord[0], coord[1], coord[2], coord[3]);
            Assert.Equal(rigth, expected);
        }

        [Theory]
        [InlineData(new double[] { 0.26, 5.84, -2.34, 11.73 }, true)]
        [InlineData(new double[] { 0, 1, 1, 1 }, false)]
        [InlineData(new double[] { 0.74, 2.87, -15, 11.73 }, true)]
        public void setPointsTest(Point3d p1, Point3d p2, Commands.LinkedPoint relation, Commands.RedefinedPoints expected)
        {
            //Console.Write("test 1 right");
            //var rigth = Commands.RightThree(0.26, 5.84, -2.34, 11.73);
            //Assert.False(rigth, "��������� �������������� ������ ���� ������ 0");
        }

        [Fact]
        public void FindRegionTest()
        {
            //Console.Write("test 1 right");
            //var rigth = Commands.RightThree(0.26, 5.84, -2.34, 11.73);
           // Assert.False(rigth, "��������� �������������� ������ ���� ������ 0");
        }

        [Fact]
        public void FindNeighborTest()
        {
            //Console.Write("test 1 right");
           // var rigth = Commands.RightThree(0.26, 5.84, -2.34, 11.73);
            //Assert.False(rigth, "��������� �������������� ������ ���� ������ 0");
        }

        [Fact]
        public void IsPointInsideTest()
        {
            //Console.Write("test 1 right");
            //var rigth = Commands.RightThree(0.26, 5.84, -2.34, 11.73);
            //Assert.False(rigth, "��������� �������������� ������ ���� ������ 0");
        }

        [Fact]
        public void NearestPointToLineTest()
        {
            //Console.Write("test 1 right");
            //var rigth = Commands.RightThree(0.26, 5.84, -2.34, 11.73);
            //Assert.False(rigth, "��������� �������������� ������ ���� ������ 0");
        }

        [Fact]
        public void DistanceTest()
        {
            //Console.Write("test 1 right");
            //var rigth = Commands.RightThree(0.26, 5.84, -2.34, 11.73);
            //Assert.False(rigth, "��������� �������������� ������ ���� ������ 0");
        }

        [Fact]
        public void PtLineDistanceTest()
        {
            //Console.Write("test 1 right");
            //var rigth = Commands.RightThree(0.26, 5.84, -2.34, 11.73);
            //Assert.False(rigth, "��������� �������������� ������ ���� ������ 0");
        }

        [Fact]
        public void PtArcDistanceTest()
        {
            //Console.Write("test 1 right");
            //var rigth = Commands.RightThree(0.26, 5.84, -2.34, 11.73);
            //Assert.False(rigth, "��������� �������������� ������ ���� ������ 0");
        }
    }
}
