﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime;
using System.ComponentModel;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;


namespace AutoCADPluginWpf
{
    public class GeometryDataLayerData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string Name { get; set; }
        string _eps;
        string _textSize;

        public string Eps
        {
            get
            {
                return _eps;
            }
            set
            {
                _eps = value.Trim();

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsValid"));
                }
            }
        }

        public string TextSize
        {
            get
            {
                return _textSize;
            }
            set
            {
                _textSize = value.Trim();

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsValid"));
                }
            }
        }

        public bool isValid
        {
            get
            {
                if (string.IsNullOrEmpty(_eps) || string.IsNullOrEmpty(_textSize))
                {
                    return false;
                }
                try
                {
                    Convert.ToDouble(_eps);
                    Convert.ToDouble(_textSize);
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }

        }

    }
}
