﻿using System.Windows;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

namespace AutoCADPluginWpf
{
    public struct HatchColor
    {
        public Point3d center;
        public System.Windows.Media.Color color;
    }
    public partial class ColorRegionWindow : Window
    {
        public System.Windows.Media.Color _Color;
        public ColorRegionWindow(System.Windows.Media.Color color)
        {
            InitializeComponent();
            _Color = color;
        }

        private void PointBth_Click(object sender, RoutedEventArgs e)
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            Point3d pt;
            Region reg;

            //PromptPointResult res = ed.GetPoint("Pick point :");
            PromptEntityResult res = ed.GetEntity("Pick entity");
            if (res.Status == PromptStatus.OK)
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    var hatchColor = new HatchColor { };
                    var entity = tr.GetObject(res.ObjectId, OpenMode.ForRead);
                    if (!(entity is Region)) { return; }
                    reg = entity as Region;

                    BlockTableRecord btr = (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);

                    Hatch hatch = new Hatch();
                    hatch.SetDatabaseDefaults();
                    hatch.SetHatchPattern(HatchPatternType.PreDefined, "SOLID");

                    hatch.Color = Autodesk.AutoCAD.Colors.Color.FromRgb(_Color.R, _Color.G, _Color.B);
                    btr.AppendEntity(hatch);
                    tr.AddNewlyCreatedDBObject(hatch, true);

                    hatch.Associative = true;
                    hatch.AppendLoop(HatchLoopTypes.Default, new ObjectIdCollection() { res.ObjectId });
                    var props = Commands.REGION_MAP[reg.ObjectId];
                    props.ColorId = hatch.Color.ColorIndex;
                    Commands.REGION_MAP[reg.ObjectId] = props;
                    hatch.EvaluateHatch(true);

                    tr.Commit();

                }
            }
        }

        private void ColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color?> e)
        {
            Editor ed = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument.Editor;
            if (sender != null && (e.NewValue is System.Windows.Media.Color valueOfE))
            {
                ed.WriteMessage("color value{0}", e.NewValue.ToString());
                _Color = valueOfE;
            }
            ed.WriteMessage("sender is null");
        }
    }
}

 