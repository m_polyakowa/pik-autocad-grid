﻿using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.PlottingServices;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using static MoreLinq.Extensions.MinByExtension;

[assembly: CommandClass(typeof(AutoCADPluginWpf.Commands))]
namespace AutoCADPluginWpf
{
    public static class Extensions
	{
		/*
		 * Найти центр произвольной фигуры
		 */
		public static Point3d GetCentroid(this Region reg, Curve cur = null)
		{
			if (cur == null)
			{
				var idc = new DBObjectCollection();
				reg.Explode(idc);
				if (idc.Count == 0)
					throw new InvalidOperationException("Cannot get cur, invalid region");

				cur = idc[0] as Curve;
			}

			if (cur == null)
				throw new InvalidOperationException("Cannot get cur, invalid region");

			var cs = cur.GetPlane().GetCoordinateSystem();
			var o = cs.Origin;
			var x = cs.Xaxis;
			var y = cs.Yaxis;

			var a = reg.AreaProperties(ref o, ref x, ref y);
			var pl = new Plane(o, x, y);
			return pl.EvaluatePoint(a.Centroid);
		}
	}

	public class Commands
	{
		public struct LinkedPoint
		{
			public Point3d point;
			public bool isLine;
			public bool isStart;
			public ArcParams? arcParams;
		}

		public struct ArcParams
		{
			public Point3d center;
			public double rad;
			public double fi1;
			public double fi2;
		}

		public struct CommonRegionProps
        {
			public List<BorderParams> borders;
			public List<Point3d> points;
		}

		public struct RegionProps 
		{
			public int num;
			public Region region;
			public Point3d center;
			public double area;
			public double borderLength;
			public List<BorderParams> borders;
			public int neighborCount;
			public List<NeighborProps> neighbors;
			public int containedCount;
			public List<RegionProps> contained;
			public int ColorId;
			public List<Point3d> points;
			public bool isCircle;
		}

		public struct NeighborProps
		{
			public int num;
			public double borderLenght;
			public double internalDist;
			public double externalDist;
		}

		private struct PolarCoords
		{
			public double rad;
			public double angle;
		}

		public struct LayersNames
		{
			public string RegionName;
			public string TextName;
		}

		public struct Params
		{
			public double Eps;
			public double TextSize;
			public string LayerName;
		}

		public struct Label
		{
			public Point3d point;
			public String text;
		}

		public struct DebugParams
		{
			public double DebugTextSize;
			public string DebugLayerName;
		}

		private struct Angles
		{
			public double angle;
			public LinkedPoint point;
		}

		public struct RedefinedPoints
		{
			public double x1;
			public double x2;
			public double y1;
			public double y2;
		}
		public struct LineParams
        {
			public Point3d p1;
			public Point3d p2;
        }

		public struct BorderParams
        {
			public ObjectId objectId;
			public bool isLine;
			public LineParams? lineParams;
			public ArcParams? arcParams;
        }
		public struct NearestPointParams
		{
			public double dist;
			public BorderParams border;
		}

		private static string LAYER;
		private static double EPS = 0.01;
		private static double TEXT_SIZE = 3;
		private static double DEBUG_TEXT_SIZE = 1;
		private static string HATCHES_LAYER_NAME = "Hatches";
		private static Dictionary<Point3d, List<LinkedPoint>> LINKED_POINTS_MARKED_MAP;
		private static readonly Document doc = Autodesk.AutoCAD.ApplicationServices.Core.Application.DocumentManager.MdiActiveDocument;
		private static readonly Editor ed = doc.Editor;
		private static readonly Database db = Autodesk.AutoCAD.ApplicationServices.Core.Application.DocumentManager.MdiActiveDocument.Database;
		private static bool WAS_COLORING = false;

		public static Dictionary<ObjectId, RegionProps> REGION_MAP;

		[CommandMethod("GETGRID")]
		public void Grid()
		{
			if (doc == null) return;
			try
			{
				var p = getGeometryParamsFromWpf();
				LAYER = p.LayerName;
				EPS = p.Eps;
				TEXT_SIZE = p.TextSize;
				var lines = GetAllLines(LAYER);
				var arcs = GetAllArcs(LAYER);
				if (lines == null && arcs == null)
				{
					throw new InvalidOperationException("На выбранном слое нет подходящих объектов");

				}
				LINKED_POINTS_MARKED_MAP = GetLinkedPointsMarkedMap(lines, arcs);
				REGION_MAP = new Dictionary<ObjectId, RegionProps> { };
				var regs = new List<List<Point3d>>();
				var rowRegs = GetRegions();
				rowRegs = NotNull(rowRegs);
				for (int i = 0; i < rowRegs.Count; i++) regs.Add(Uniq(rowRegs[i]));

				regs = UniqList(regs);
				var names = getLayersNamesFromWpf();
				CreateLayer(names.RegionName, 0);
				var elems = CreateLines(regs);
				var regions = CreateRegion(elems);
				var result = FindNeighbor(regions);
				CreateLayer(names.TextName, 0);
				CreateLabelsForRegions(result);
				var file = toFileHandle(WAS_COLORING);
				writeToFile(file);
			}
			catch(System.Exception ex)
            {
				ed.WriteMessage("Ошибка: {0}", ex);
            }
		}

		[CommandMethod("COLORREGION")]
		public void ColorRegion()
        {
			var color = System.Windows.Media.Color.FromRgb(255, 0, 0);
			var window = new ColorRegionWindow(color);
			CreateLayer(HATCHES_LAYER_NAME, 0);
			WAS_COLORING = true;
			if (Autodesk.AutoCAD.ApplicationServices.Core.Application.ShowModalWindow(window) != true)
			{
				ed.WriteMessage("Присвоено имя слоя по умолчанию - 0");
			}
		}

		[CommandMethod("ENABLEDEBUG")]
		public void EnableDebug()
		{
			DebugParams p = getDebugInfoFromWpf();
			DEBUG_TEXT_SIZE = p.DebugTextSize;
			var debugLayerName = p.DebugLayerName;
			var lines = GetAllLines(debugLayerName);
			var arcs = GetAllArcs(debugLayerName);
			if (lines == null && arcs == null)
			{
				throw new InvalidOperationException("На выбранном слое нет подходящих объектов");

			}
			LINKED_POINTS_MARKED_MAP = GetLinkedPointsMarkedMap(lines, arcs);
			CreateLabelsForPoints();
		}

		[CommandMethod("WRITETOFILE")]
		public void writeToFileCommand()
        {
			var file = toFileHandle(WAS_COLORING);
			writeToFile(file);
        }

		public static Params getGeometryParamsFromWpf()
        {
			var defaultParams = new GeometryDataLayerData { Name = "0", Eps = "0.02", TextSize = "0.1" };
			var window = new GeometryDataLayerNameInput(defaultParams);
			if (Autodesk.AutoCAD.ApplicationServices.Core.Application.ShowModalWindow(window) != true)
			{
				ed.WriteMessage("Присвоено имя слоя по умолчанию - 0");
				var par = new Params
				{
					LayerName = defaultParams.Name,
					Eps = Convert.ToDouble(defaultParams.Eps),
					TextSize = Convert.ToDouble(defaultParams.TextSize),
				};
				return par;
			}
			ed.WriteMessage("defaultName {0}", defaultParams);
			var p = new Params
			{
				LayerName = defaultParams.Name,
				Eps = Convert.ToDouble(defaultParams.Eps),
				TextSize = Convert.ToDouble(defaultParams.TextSize),
			};
			return p;
		}

		public static LayersNames getLayersNamesFromWpf()
        {
			var data = new LayersData();
			var names = new LayersNames
			{
				RegionName = "Regions",
				TextName = "Text",
			};
			data.RegionLayerName = names.RegionName;
			data.TextLayerName = names.TextName;
			var window = new LayersNameInput(data);
			if (Autodesk.AutoCAD.ApplicationServices.Core.Application.ShowModalWindow(window) != true)
			{
				ed.WriteMessage("Имена не были выбраны, слоям были присвоены дефолтные имена");
				return names;
			}
			names.RegionName = data.RegionLayerName;
			names.TextName = data.TextLayerName;
			return names;
        }

		public static DebugParams getDebugInfoFromWpf()
        {
			var defaultParams = new DebugInfoData { DebugLayerName = "0", DebugTextSize = "0.07" };
			var window = new DebugInfo(defaultParams);
			if (Autodesk.AutoCAD.ApplicationServices.Core.Application.ShowModalWindow(window) != true)
			{
				ed.WriteMessage("Присвоено имя слоя по умолчанию - 0");
				var par = new DebugParams
				{
					DebugLayerName = defaultParams.DebugLayerName,
					DebugTextSize = Convert.ToDouble(defaultParams.DebugTextSize),
				};
				return par;
			}
			ed.WriteMessage("defaultName {0}", defaultParams);
			var p = new DebugParams
			{
				DebugLayerName = defaultParams.DebugLayerName,
				DebugTextSize = Convert.ToDouble(defaultParams.DebugTextSize),
			};
			return p;
		}

		public static ObjectIdCollection SelectAllCADObjectsByLayer(string Name, string sLayer)
		{

			ObjectIdCollection retVal = null;

			try
			{
				// Get a selection set of all possible polyline entities on the requested layer
				PromptSelectionResult oPSR = null;

				var tvs = new TypedValue[] {
					new TypedValue(Convert.ToInt32(DxfCode.Operator), "<and"),
					new TypedValue(Convert.ToInt32(DxfCode.LayerName), sLayer),
					new TypedValue(Convert.ToInt32(DxfCode.Operator), "<or"),
					new TypedValue(Convert.ToInt32(DxfCode.Start), Name),
					new TypedValue(Convert.ToInt32(DxfCode.Operator), "or>"),
					new TypedValue(Convert.ToInt32(DxfCode.Operator), "and>")
				};

				var oSf = new SelectionFilter(tvs);

				oPSR = ed.SelectAll(oSf);

				if (oPSR.Status == PromptStatus.OK)
				{
					retVal = new ObjectIdCollection(oPSR.Value.GetObjectIds());
				}
				else
				{
					retVal = new ObjectIdCollection();
				}
			}
			catch (System.Exception ex)
			{
				// ReportError(ex);
			}

			return retVal;
		}

		public static Line[] GetAllLines(string layer)
		{
			var lines = SelectAllCADObjectsByLayer(Name: "LINE", sLayer: layer);
			var arr = new List<Line> { };
			using (Transaction trans = db.TransactionManager.StartTransaction())
			{
				for (int i = 0; i < lines.Count; i++)
				{
					var lineId = lines[i];
					var l = trans.GetObject(lineId, OpenMode.ForRead) as Line;
					arr.Add(l);
				}
			}
			arr = UniqLine(arr);
			return arr.ToArray();
		}

		public static Arc[] GetAllArcs(string layer)
		{
			var arcs = SelectAllCADObjectsByLayer(Name: "ARC", sLayer: layer);
			var arr = new List<Arc> { };
			for (int i = 0; i < arcs.Count; i++)
			{
				var arcId = arcs[i];

				using (Transaction trans = db.TransactionManager.StartTransaction())
				{
					var a = trans.GetObject(arcId, OpenMode.ForRead) as Arc;

					arr.Add(a);
				}
			}
			return arr.ToArray();
		}

		public static bool RightThree(double diffX1, double diffY1, double diffX2, double diffY2)
		{
			var tmp = diffX1*diffY2 - diffX2*diffY1;
			return tmp >= 0;
		}

		public static RedefinedPoints setPoints(Point3d p1, Point3d p2, LinkedPoint relation)
        {
			var x1 = p1.X;
			var y1 = p1.Y;
			var x2 = p2.X;
			var y2 = p2.Y;
			if (relation.isLine == false && relation.arcParams.HasValue)
			{
				var arcParams = relation.arcParams.GetValueOrDefault();
				double x0 = arcParams.center.X;
				double y0 = arcParams.center.Y;
				double rad = arcParams.rad;

				//if (Math.Abs(y2 - y0) > EPS)
				//{
				//	x1 = 0;
				//	y1 = (Math.Pow(rad, 2) - Math.Pow(x0, 2) - Math.Pow(y0,2) + x0*x1 + y0*y1) / (y2 - y0);
				//	if (IsEqualPoints(new Point3d(x1, y1, 0), p2)) x1 = 1;
				//}
				//else
				//{
				//	y1 = 0;
				//	x1 = (Math.Pow(rad, 2) - Math.Pow(x0, 2) - Math.Pow(y0, 2) + x0 * x1 + y0 * y1) / (x2 - x0);
				//	if (IsEqualPoints(new Point3d(x1, y1, 0), p2)) y1 = 1;
				//}

				if (Math.Abs(x2 - x0) > EPS)
				{
					y1 = 0;
					x1 = (Math.Pow(rad, 2) - Math.Pow(x0, 2) - Math.Pow(y0, 2) + x0 * x1 + y0 * y1) / (x2 - x0);
					if (IsEqualPoints(new Point3d(x1, y1, 0), p2)) y1 = 1;
				}
				else
				{
					x1 = 0;
					y1 = (Math.Pow(rad, 2) - Math.Pow(x0, 2) - Math.Pow(y0, 2) + x0 * x1 + y0 * y1) / (y2 - y0);
					if (IsEqualPoints(new Point3d(x1, y1, 0), p2)) x1 = 1;
				}
			}
			return new RedefinedPoints { x1 = x1, x2 = x2, y1 = y1, y2 = y2 };
		}
		private static List<Point3d> FindRegion(List<Point3d> reg, List<LinkedPoint> pts, LinkedPoint lastTwoRelation)
		{
			var empty = new List<Point3d> { };

			////// удалить потом
			//ed.WriteMessage("\nReg:\n");
			//ed.WriteMessage(JsonConvert.SerializeObject(reg));
			//ed.WriteMessage("\nPts:\n");
			//ed.WriteMessage(JsonConvert.SerializeObject(pts));
			//ed.WriteMessage("\nlastTwoRelation:\n");
			//ed.WriteMessage(JsonConvert.SerializeObject(lastTwoRelation));
			//ed.WriteMessage("\nLINKED_POINTS_MARKED_MAP:\n");
			//ed.WriteMessage(JsonConvert.SerializeObject(LINKED_POINTS_MARKED_MAP));

			//////

			while (!IsEqualPoints(reg[0], reg[reg.Count - 1]))
			{
				if (reg.Count > 30) { return empty; }
				RedefinedPoints redefinedPoints1 = setPoints(reg[reg.Count - 2], reg[reg.Count - 1], lastTwoRelation);
				var x1 = redefinedPoints1.x1;
				var y1 = redefinedPoints1.y1;
				var x21 = redefinedPoints1.x2;
				var y21 = redefinedPoints1.y2;

				var diffX1 = x21 - x1;
				var diffY1 = y21 - y1;
				if (((reg[reg.Count - 1].X - reg[reg.Count - 2].X <= 0 && x21 - x1 >= 0) || (reg[reg.Count - 1].X - reg[reg.Count - 2].X >= 0 && x21 - x1 <= 0))
				&& ((reg[reg.Count - 1].Y - reg[reg.Count - 2].Y <= 0 && y21 - y1 >= 0) || (reg[reg.Count - 1].Y - reg[reg.Count - 2].Y >= 0 && y21 - y1 <= 0)))
				{
					diffX1 = x1 - x21;
					diffY1 = y1 - y21;
				}

				var angles = new List<Angles>();

				for (int i = 0; i < pts.Count; i++)
				{
					if (IsEqualPoints(reg[reg.Count - 2], pts[i].point)) continue;
					RedefinedPoints redefinedPoints2 = setPoints(reg[reg.Count - 1], pts[i].point, pts[i]);
					var x22 = redefinedPoints2.x1;
					var y22 = redefinedPoints2.y1;
					var x3 = redefinedPoints2.x2;
					var y3 = redefinedPoints2.y2;

					var diffX2 = x3 - x22;
					var diffY2 = y3 - y22;

					var diffX3 = x3 - x1;
					var diffY3 = x3 - x1;

					if (((pts[i].point.X - reg[reg.Count - 1].X <= 0 && x3 - x22 >= 0) || (pts[i].point.X - reg[reg.Count - 1].X >= 0 && x3 - x22 <= 0)) 
						&& ((pts[i].point.Y - reg[reg.Count - 1].Y <= 0 && y3 - y22 >= 0) || (pts[i].point.Y - reg[reg.Count - 1].Y >= 0 && y3 - y22 <= 0)))
					{
						diffX2 = x22 - x3;
						diffY2 = y22 - y3;
						diffX3 = x1 - x3;
						diffY3 = x1 - x3;
					}
					var right = RightThree(diffX1, diffY1, diffX2, diffY2);

                    // костыль для случая когда дуга делится больше чем на две части
                    if (!pts[i].isLine &&
                        !lastTwoRelation.isLine &&
                        pts[i].arcParams.HasValue &&
                        lastTwoRelation.arcParams.HasValue &&
                        IsEqualPoints(pts[i].arcParams.GetValueOrDefault().center, lastTwoRelation.arcParams.GetValueOrDefault().center) &&
                        Math.Abs(pts[i].arcParams.GetValueOrDefault().rad - lastTwoRelation.arcParams.GetValueOrDefault().rad) < EPS && !IsEqualPoints(reg[0], pts[i].point))
                    {
                        right = true;
                    }

                    var cos = (diffX1 * diffX2 + diffY1 * diffY2) / (Math.Sqrt(Math.Pow(diffX1, 2) + Math.Pow(diffY1, 2)) * Math.Sqrt(Math.Pow(diffX2, 2) + Math.Pow(diffY2, 2)));
					if (Math.Abs(cos) > 1) cos = Math.Sign(cos);
					var angle = Math.PI - Math.Acos(cos);
					if (right && angle <= Math.PI + EPS && Math.Abs(angle) >= EPS)
					{
						var a = new Angles { angle = angle, point = pts[i] };
						angles.Add(a);
					}
				}
				var minPt = angles.OrderBy(i => i.angle).FirstOrDefault();
				if (!object.Equals(minPt, default(Angles)))
				{
					reg.Add(minPt.point.point);
					pts = GetNeighborMarkedPointOrNull(LINKED_POINTS_MARKED_MAP, minPt.point.point);
					lastTwoRelation = minPt.point;
				}
				else return empty;
			}
			////// удалить потом
			//ed.WriteMessage("\nResult:\n");
			//ed.WriteMessage(JsonConvert.SerializeObject(reg));

			//////
			if (reg.Count > 2) return reg;
			else return empty;

		}

		public static List<List<Point3d>> GetRegions()
		{
			var regs = new List<List<Point3d>>();
			foreach (Point3d pt in LINKED_POINTS_MARKED_MAP.Keys)
			{
				var linkedPts = LINKED_POINTS_MARKED_MAP[pt];
				for (int j = 0; j < linkedPts.Count; j++)
				{
					var reg = new List<Point3d> { pt, linkedPts[j].point };
					var pts1 = GetNeighborMarkedPointOrNull(LINKED_POINTS_MARKED_MAP, linkedPts[j].point);
					var tmp = FindRegion(reg, pts1, linkedPts[j]);
					regs.Add(tmp);
				}
			}
			return regs;
		}

		private static Dictionary<Point3d, List<LinkedPoint>> GetLinkedPointsMarkedMap(Line[] lines, Arc[] arcs)
		{
			var map = new Dictionary<Point3d, List<LinkedPoint>>();
			try
			{
				for (int i = 0; i < lines.Length; i++)
				{
					var l = lines[i];
					var startPoint = l.StartPoint;
					var endPoint = l.EndPoint;

					var neighborStartPoint = GetNeighborMarkedPointOrNull(map, startPoint);
					LinkedPoint p1;
					p1.point = endPoint;
					p1.isLine = true;
					p1.isStart = true;
					p1.arcParams = null;
					if (neighborStartPoint != null) neighborStartPoint.Add(p1);
					else map[startPoint] = new List<LinkedPoint> { p1 };

					var neighborEndPoint = GetNeighborMarkedPointOrNull(map, endPoint);
					LinkedPoint p2;
					p2.point = startPoint;
					p2.isLine = true;
					p2.isStart = false;
					p2.arcParams = null;
					if (neighborEndPoint != null) neighborEndPoint.Add(p2);
					else map[endPoint] = new List<LinkedPoint> { p2 };
				}

				for (int j = 0; j < arcs.Length; j++)
				{
					Arc a = arcs[j];
					var startPoint = GetPoint3dFromPolar(a.Radius, a.StartAngle);
					var endPoint = GetPoint3dFromPolar(a.Radius, a.EndAngle);

					var neighborStartPoint = GetNeighborMarkedPointOrNull(map, startPoint);
					LinkedPoint p1;
					p1.point = endPoint;
					p1.isLine = false;
					p1.isStart = true;
					p1.arcParams = new ArcParams { center = a.Center, rad = a.Radius };

					if (neighborStartPoint != null) neighborStartPoint.Add(p1);
					else map[startPoint] = new List<LinkedPoint> { p1 };

					var neighborEndPoint = GetNeighborMarkedPointOrNull(map, endPoint);
					LinkedPoint p2;
					p2.point = startPoint;
					p2.isLine = false;
					p2.isStart = false;
					p2.arcParams = new ArcParams { center = a.Center, rad = a.Radius };

					if (neighborEndPoint != null) neighborEndPoint.Add(p2);
					else map[endPoint] = new List<LinkedPoint> { p2 };
				}
				foreach (Point3d key in map.Keys)
				{
					map[key] = UniqMarked(map[key]);
				}
			}
			catch(System.Exception ex)
            {
				ed.WriteMessage("{0}", ex);
            }

			return map;
		}

		/*
		 * Получить смежные точки
		 */
		private static List<LinkedPoint> GetNeighborMarkedPointOrNull(Dictionary<Point3d, List<LinkedPoint>> map, Point3d pt)
		{
			foreach (Point3d k in map.Keys)
			{
				if (IsEqualPoints(k, pt)) return map[k];
			}
			return null;
		}

		private static bool IsEqualPoints(Point3d pt1, Point3d pt2)
		{
			var tolerance = new Tolerance (EPS, EPS);
			return pt1.IsEqualTo(pt2, tolerance);
		}
		private static List<List<Point3d>> UniqList(List<List<Point3d>> lst)
		{
			for (int i = 0; i < lst.Count; i++)
			{
				var lsti = lst[i];
				int leni = lsti.Count;
				for (int j = i + 1; j < lst.Count; j++)
				{
					var lstj = lst[j];
					int lenj = lstj.Count;
					if (leni == lenj)
					{
						int count = 0;
						for (int k = 0; k < leni; k++)
						{
							for (int l = 0; l < lenj; l++)
							{
								if (IsEqualPoints(lsti[k], lstj[l])) count++;
							}
						}
						if (count == leni)
						{
							lst.RemoveAt(j);
							j--;
						}
					}
				}
			}
			return lst;
		}
		private static List<Point3d> Uniq(List<Point3d> lst)
		{
			for (int i = 0; i < lst.Count; i++)
			{
				for (int j = i + 1; j < lst.Count; j++)
				{
					if (IsEqualPoints(lst[i], lst[j]))
					{
						lst.RemoveAt(j);
						j--;
					}
				}
			}

			return lst;
		}

		private static List<LinkedPoint> UniqMarked(List<LinkedPoint> lst)
		{
			for (int i = 0; i < lst.Count; i++)
			{
				for (int j = i + 1; j < lst.Count; j++)
				{
					if (IsEqualPoints(lst[i].point, lst[j].point))
					{
						lst.RemoveAt(j);
						j--;
					}
				}
			}

			return lst;
		}


		private static List<List<Point3d>> NotNull(List<List<Point3d>> lst)
		{
			return lst.FindAll(elem => elem.Count != 0);
		}
		
		private static List<Line> UniqLine(List<Line> lst)
		{
			using (var tr = db.TransactionManager.StartTransaction())
			{
				var bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				for (int i = 0; i < lst.Count; i++)
				{
					var l = lst[i];
					for (int j = 0; j < lst.Count; j++)
					{
						Line l1 = lst[j];
						if (i != j 
							&& 
							((IsEqualPoints(l.StartPoint, l1.StartPoint) && IsEqualPoints(l.EndPoint, l1.EndPoint)) 
							|| 
							(IsEqualPoints(l.StartPoint, l1.EndPoint) && IsEqualPoints(l.EndPoint, l1.StartPoint))))
						{
							var ent = tr.GetObject(l1.ObjectId, OpenMode.ForRead) as Line;
							if (ent != null)
							{
								ent.UpgradeOpen();
								ent.Erase();
								ent.DowngradeOpen();
							}
							lst.Remove(l1);
						}
					}
				}
				tr.Commit();
			}
			return lst;
		}


		private void CreateLayer(String Name, short ColorIndex)
		{
			var tr = db.TransactionManager.StartTransaction();
			using (tr)
			{
				var lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForRead);

				if (lt.Has(Name))
				{
					ed.WriteMessage("\nA layer with this name already exists.");
					return;
				}

				var ltr = new LayerTableRecord();
				ltr.Name = Name;
				ltr.Color = Color.FromColorIndex(ColorMethod.ByAci, ColorIndex);
				lt.UpgradeOpen();
				var ltId = lt.Add(ltr);
				tr.AddNewlyCreatedDBObject(ltr, true);
				db.Clayer = ltId;
				tr.Commit();
				ed.WriteMessage("\nCreated layer named \"{0}\" with " + "a color index of {1}.", Name, ColorIndex++);
			}
		}
		private List<CommonRegionProps> CreateLines(List<List<Point3d>> regs)
		{
			var res = new List<CommonRegionProps> { };
			var tr = db.TransactionManager.StartTransaction();
			using (tr)
			{
				var BlockTable = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				var TableRecord = tr.GetObject(BlockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
				foreach (List<Point3d> reg in regs)
				{
					var commonRegion = new CommonRegionProps { points = reg};
					var borderParams = new List<BorderParams>();
					for (int i = 0; i < reg.Count; i++)
					{
						try
						{
							var pt1 = reg[i];
							var pt2 = reg[(i + 1) % reg.Count];
							var hasPoint = false;
							var isLine = true;
							var isStart = false;
							var pts = GetNeighborMarkedPointOrNull(LINKED_POINTS_MARKED_MAP, pt1);
							foreach (LinkedPoint e in pts)
							{
								if (IsEqualPoints(e.point, pt2))
								{
									isLine = e.isLine;
									isStart = e.isStart;
									hasPoint = true;
								}
							}
//							if (!hasPoint)
//							{
//								throw new InvalidOperationException("не удалось найти точку");
//							}
							if (isLine == true)
							{
								Line acLine = new Line(pt1, pt2);
								acLine.SetDatabaseDefaults();
								TableRecord.AppendEntity(acLine);
								tr.AddNewlyCreatedDBObject(acLine, true);
								borderParams.Add(new BorderParams { objectId = acLine.ObjectId, isLine = true, lineParams = new LineParams{ p1 = pt1, p2 = pt2 } });
							}
							else if (isLine == false)
							{
								var center = new Point3d(0, 0, 0);
								double dist = Math.Sqrt(Math.Pow(center.X - pt1.X, 2) + Math.Pow(center.Y - pt1.Y, 2));
								double fi1;
								double fi2;
								var cos1 = pt1.X / dist;
								var cos2 = pt2.X / dist;

								if (Math.Abs(cos1) > 1) cos1 = Math.Sign(cos1);
								if (Math.Abs(cos2) > 1) cos2 = Math.Sign(cos2);
								
								if (pt1.Y > 0) fi1 = Math.Acos(cos1);
								else fi1 = -Math.Acos(cos1);

								if (pt2.Y > 0) fi2 = Math.Acos(cos2);
								else fi2 = -Math.Acos(cos2);

								if (isStart == true)
								{
									var acArc = new Arc(center, dist, fi1, fi2);
									acArc.SetDatabaseDefaults();
									TableRecord.AppendEntity(acArc);
									tr.AddNewlyCreatedDBObject(acArc, true);
									//tmp.Add(tr.GetObject(acArc.ObjectId, OpenMode.ForRead));
									borderParams.Add(new BorderParams { objectId = acArc.ObjectId, isLine = false, arcParams = new ArcParams { center = center, rad = dist, fi1 = fi1, fi2 = fi2} });
								}
								else
								{
									var acArc = new Arc(center, dist, fi2, fi1);
									acArc.SetDatabaseDefaults();
									TableRecord.AppendEntity(acArc);
									tr.AddNewlyCreatedDBObject(acArc, true);
									borderParams.Add(new BorderParams { objectId = acArc.ObjectId, isLine = false, arcParams = new ArcParams { center = center, rad = dist, fi1 = fi2, fi2 = fi1 } });
								}
							}
						}
						catch (Autodesk.AutoCAD.Runtime.Exception ex)
						{
							ed.WriteMessage("line or arc creating error {0}\n", ex);
						}
					}
					commonRegion.borders = borderParams;
					res.Add(commonRegion);
				}
				tr.Commit();
			}
			return res;
		}

		private List<RegionProps> CreateRegion(List<CommonRegionProps> lst)
		{
			var regs = new List<RegionProps> { };
			int i = 0;
			var tr = db.TransactionManager.StartTransaction();
			using (tr)
			{
				foreach (CommonRegionProps commonReg in lst)
				{
					var borders = commonReg.borders;
					var collection = new DBObjectCollection { };
					foreach (BorderParams borderParams in borders)
					{
						collection.Add(tr.GetObject(borderParams.objectId, OpenMode.ForRead));
					}
					var isCircle = borders.All(b => !b.isLine) && borders.Any(o => o.arcParams.GetValueOrDefault().rad != borders[0].arcParams.GetValueOrDefault().rad);
					var BlockTable = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
					var TableRecord = tr.GetObject(BlockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
					ed.WriteMessage("Db collection count {0}\n", collection.Count);
					try
					{ 
						var regions = Region.CreateFromCurves(collection);
						var curSpace = (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);
						foreach (Region region in regions)
						{
							RegionProps props = new RegionProps { };
							curSpace.AppendEntity(region);
							tr.AddNewlyCreatedDBObject(region, true);
							props.num = i;
							props.borders = borders;
							props.region = region;
							props.area = region.Area;
							props.borderLength = region.Perimeter;
							props.center = region.GetCentroid();
							props.neighborCount = 0;
							props.neighbors = new List<NeighborProps> { };
							props.points = commonReg.points;
							props.isCircle = isCircle;
							regs.Add(props);
							REGION_MAP[region.ObjectId] = props;
							i++;
						}
					}
					catch (Autodesk.AutoCAD.Runtime.Exception ex)
					{
						ed.WriteMessage("region error {0} i - {1}\n", ex, i);
					}
				}
					tr.Commit();
			}
			return regs;
		}

		private void CreateLabelsForRegions(List<RegionProps> regions)
		{
			var labels = new List<Label> { };
			foreach (RegionProps props in regions)
			{
				var label = new Label { text = (props.num + 1).ToString(), point = props.center };
				labels.Add(label);
			}
			CreateText(labels, TEXT_SIZE);
		}

		private void CreateLabelsForPoints()
		{
			var labels = new List<Label> { };
			foreach (Point3d point in LINKED_POINTS_MARKED_MAP.Keys)
			{
				var text = $"{point.X.ToString("0.00")},{point.Y.ToString("0.00")}";
				var label = new Label { text = text, point = point };
				labels.Add(label);
			}
			CreateText(labels, DEBUG_TEXT_SIZE);
		}

		private void CreateText(List<Label> labels, double textSize)
		{
			var tr = db.TransactionManager.StartTransaction();
			try
			{
				using (tr)
				{
					var BlockTable = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
					var TableRecord = tr.GetObject(BlockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
					foreach (Label label in labels)
					{
						var acText = new DBText();
						var acPoint = new DBPoint(label.point);
						acText.SetDatabaseDefaults();
						acText.Position = label.point;
						acText.Height = textSize;
						acText.TextString = label.text;
						TableRecord.AppendEntity(acText);
						TableRecord.AppendEntity(acPoint);
						tr.AddNewlyCreatedDBObject(acText, true);
						tr.AddNewlyCreatedDBObject(acPoint, true);
					}
					tr.Commit();
				}
			}
			catch (Autodesk.AutoCAD.Runtime.Exception ex)
			{
				ed.WriteMessage("text creating error {0}\n", ex);
			}
		}

		private List<RegionProps> FindNeighbor(List<RegionProps> lst)
		{
			//ed.WriteMessage("\nNeighbor start lst:\n");
			//ed.WriteMessage(JsonConvert.SerializeObject(lst));
			try
			{
				var tr = db.TransactionManager.StartTransaction();
				using (tr)
				{
					for (int i = 0; i < lst.Count; i++)
					{
						var props = lst[i];
						var borders = props.borders;
						for (int j = i + 1; j < lst.Count; j++)
						{
							var props1 = lst[j];
							var borders1 = props1.borders;
							if (props.num != props1.num && borders.Count == borders.Count)
							{
								for (int k = 0; k < borders.Count; k++)
								{
									var b1 = tr.GetObject(borders[k].objectId, OpenMode.ForRead) as Entity;
									for (int m = 0; m < borders1.Count; m++)
									{
										var b2 = tr.GetObject(borders1[m].objectId, OpenMode.ForRead) as Entity;
										if (b1 is Line && b2 is Line)
										{
											var l1 = tr.GetObject(borders[k].objectId, OpenMode.ForRead) as Line;
											var l2 = tr.GetObject(borders1[m].objectId, OpenMode.ForRead) as Line;
											if (CheckEqualLines(l1, l2))
											{
												props.neighborCount += 1;
												props1.neighborCount += 1;
												var neighborprops = new NeighborProps { };
												neighborprops.num = props1.num;
												neighborprops.borderLenght = l1.Length;
												neighborprops.internalDist = PtLineDistance(props.center, l1);
												neighborprops.externalDist = PtLineDistance(props1.center, l1);
												props.neighbors.Add(neighborprops);
												neighborprops.num = props.num;
												neighborprops.internalDist = PtLineDistance(props1.center, l1);
												neighborprops.externalDist = PtLineDistance(props.center, l1);
												props1.neighbors.Add(neighborprops);
											}
										}
										if (b1 is Arc && b2 is Arc)
										{
											var a1 = tr.GetObject(borders[k].objectId, OpenMode.ForRead) as Arc;
											var a2 = tr.GetObject(borders1[m].objectId, OpenMode.ForRead) as Arc;
											if (CheckEqualArcs(a1, a2))
											{
												props.neighborCount += 1;
												props1.neighborCount += 1;
												NeighborProps neighborprops = new NeighborProps { };
												neighborprops.num = props1.num;
												neighborprops.borderLenght = a1.Length;
												neighborprops.internalDist = PtArcDistance(props.center, a1);
												neighborprops.externalDist = PtArcDistance(props1.center, a1);
												props.neighbors.Add(neighborprops);
												neighborprops.num = props.num;
												neighborprops.internalDist = PtArcDistance(props1.center, a1);
												neighborprops.externalDist = PtArcDistance(props.center, a1);
												props1.neighbors.Add(neighborprops);
											}
										}
									}
								}
							}
						}
					}
				}
            }
			catch (Autodesk.AutoCAD.Runtime.Exception ex)
			{
				ed.WriteMessage("region error {0}\n", ex);
			}
            for (int i = 0; i < lst.Count; i++)
            {
                var props = lst[i];
                if (props.neighbors.Count == 0)
                {
                    for (int j = 0; j < lst.Count; j++)
                    {
                        if (i == j) continue;
						var props1 = lst[j];
                        var isAllPointsInside = props.points.All(pt => props1.points.All(regPt => !IsEqualPoints(pt, regPt)) && IsPointInside(pt, props1));
                        if (isAllPointsInside)
                        {
                            ed.WriteMessage("yes points from num {0} inside num {1}\n", lst[i].num + 1, lst[j].num + 1);
							props.neighborCount += 1;
							props1.neighborCount += 1;
							NeighborProps neighborprops = new NeighborProps { };
							neighborprops.num = props1.num;
							neighborprops.borderLenght = props.borderLength;
							neighborprops.internalDist = props.borders.Min(border => PtEntityDistance(props.center, border.objectId));
							neighborprops.externalDist = props.borders.Min(border => PtEntityDistance(props1.center, border.objectId));
							props.neighbors.Add(neighborprops);
							neighborprops.num = props.num;
							neighborprops.internalDist = props.borders.Min(border => PtEntityDistance(props1.center, border.objectId));
							neighborprops.externalDist = props.borders.Min(border => PtEntityDistance(props.center, border.objectId));
							props1.neighbors.Add(neighborprops);
						}
                    }
                }
            }
            //ed.WriteMessage("\nNeighbor end lst:\n");
            //ed.WriteMessage(JsonConvert.SerializeObject(lst));
            return lst;
		}

		private bool IsPointInside(Point3d pt, RegionProps reg)
        {
			var tr = db.TransactionManager.StartTransaction();
			try
			{
				using (tr)
				{
					var probablyCircle = tr.GetObject(reg.borders[0].objectId, OpenMode.ForRead) as Entity;
					var dists = new Dictionary<Point3d, NearestPointParams>();
					Vector3d normal;
					//if (reg.borders.Count == 1 && probablyCircle is Circle)
					//{
					//	var c = tr.GetObject(reg.borders[0].objectId, OpenMode.ForRead) as Circle;
					//	var center = c.Center;
					//	normal = new Vector3d(pt.X - center.X, pt.Y - center.Y, 0);
					//	var l = new Line(pt, center);
					//	var points = findLineCircleIntersectPoint(l, c);
					//	double[] distans = new double[] { };
					//	if (points != null)
					//	{
					//		foreach (Point3d point in points)
					//		{
					//			distans.Append(Distance(pt, point));
					//		}
					//		int idx = Array.FindIndex(distans, e => e == distans.Min());
					//		Vector3d vec = new Vector3d(points[idx].X - pt.X, points[idx].Y - pt.Y, 0);
					//		if (vec.GetAngleTo(normal) < Math.PI / 2) return true;
					//		return false;
					//	}
					//	return false;
					//}

					// find the nearest point to border
					foreach (BorderParams borderParams in reg.borders)
					{
						var border = tr.GetObject(borderParams.objectId, OpenMode.ForRead) as Entity;
						if (border is Line)
						{
							var line = border as Line;
							var localClosest = line.GetClosestPointTo(pt, false);
							var param = new NearestPointParams { dist = Distance(localClosest, pt), border = borderParams };
							dists[localClosest] = param;
						}
						if (border is Arc)
						{
							var arc = border as Arc;
							var localClosest = arc.GetClosestPointTo(pt, false);
							var param = new NearestPointParams { dist = Distance(localClosest, pt), border = borderParams };
							dists[localClosest] = param;
						}
					}

					var closest = dists.Aggregate((l, r) => l.Value.dist < r.Value.dist ? l : r).Key;
					normal = GetNormal(closest, dists[closest].border, reg);

					var v = new Vector3d(closest.X - pt.X, closest.Y - pt.Y, 0);
					if (v.GetAngleTo(normal) < Math.PI / 2) return true;
					return false;
				}
			}
			catch (Autodesk.AutoCAD.Runtime.Exception ex)
			{
				ed.WriteMessage("is point inside error {0}\n", ex);
			}
		
			return false;
        }

		public static Vector3d GetNormal (Point3d localClosest, BorderParams border, RegionProps reg)
		{
			Vector3d normal = new Vector3d();
			Point3d startPoint;
			Point3d endPoint;
			try
			{
				if (border.isLine && border.lineParams.HasValue)
				{
					var lineParams = border.lineParams.GetValueOrDefault();
					startPoint = lineParams.p1;
					endPoint = lineParams.p2;
					var vectorizedLine = new Vector3d(lineParams.p2.X - lineParams.p1.X, lineParams.p2.Y - lineParams.p1.Y, 0);
					var codirectional = new Vector3d(localClosest.X - reg.center.X, localClosest.Y - reg.center.Y, 0);
					normal = vectorizedLine.GetPerpendicularVector();
					if (normal.GetAngleTo(codirectional) > Math.PI) normal = normal.Negate();
				}
				else if (border.arcParams.HasValue)
				{
					var arcParams = border.arcParams.GetValueOrDefault();
					startPoint = GetPoint3dFromPolar(arcParams.rad, arcParams.fi1);
					endPoint = GetPoint3dFromPolar(arcParams.rad, arcParams.fi2);
					//var codirectional = new Vector3d(localClosest.X - reg.center.X, localClosest.Y - reg.center.Y, 0);
					normal = GetArcNormal(localClosest, arcParams.center, arcParams.rad, reg);
					//if (normal.GetAngleTo(codirectional) > Math.PI) normal = normal.Negate();
				}
				else throw new InvalidOperationException("Нет параметров границы");

				if (IsEqualPoints(startPoint, localClosest) || IsEqualPoints(endPoint, localClosest))
				{
					var relatedBorders = reg.borders.FindAll(entity =>
					{
						if (entity.isLine && entity.lineParams.HasValue)
						{
							var lineParams = entity.lineParams.GetValueOrDefault();
							if (IsEqualPoints(lineParams.p1, localClosest) || IsEqualPoints(lineParams.p2, localClosest)) return true;
							return false;
						}
						else if (entity.arcParams.HasValue)
						{
							var arcParams = entity.arcParams.GetValueOrDefault();
							if (IsEqualPoints(GetPoint3dFromPolar(arcParams.rad, arcParams.fi1), localClosest) ||
							IsEqualPoints(GetPoint3dFromPolar(arcParams.rad, arcParams.fi2), localClosest)) return true;
							return false;
						}
						return false;
					});
					if (relatedBorders.Count != 2) throw new InvalidOperationException("У точки должны быть две смежные границы");
					var border1 = relatedBorders[0];
					var border2 = relatedBorders[1];
					Vector3d vec1;
					Vector3d vec2;
					if (border1.isLine && border1.lineParams.HasValue)
					{
						var lineParams = border1.lineParams.GetValueOrDefault();
						if (IsEqualPoints(lineParams.p1, localClosest)) vec1 = new Vector3d(localClosest.X - lineParams.p2.X, localClosest.Y - lineParams.p2.Y, 0);
						else vec1 = new Vector3d(localClosest.X - lineParams.p1.X, localClosest.Y - lineParams.p1.Y, 0);
					}
					else if (border1.arcParams.HasValue)
					{
						var arcParams = border1.arcParams.GetValueOrDefault();
						var p1 = GetPoint3dFromPolar(arcParams.rad, arcParams.fi1);
						var p2 = GetPoint3dFromPolar(arcParams.rad, arcParams.fi2);
						Vector3d codirectional;
						vec1 = new Vector3d(arcParams.center.X - localClosest.X, arcParams.center.Y - localClosest.Y, 0).GetPerpendicularVector();
						if (IsEqualPoints(p1, localClosest)) codirectional = new Vector3d(localClosest.X - p2.X, localClosest.Y - p2.Y, 0);
						else codirectional = new Vector3d(localClosest.X - p1.X, localClosest.Y - p1.Y, 0);
						if (vec1.GetAngleTo(codirectional) > Math.PI / 2) vec1 = vec1.Negate();
						var t = vec1.GetAngleTo(codirectional);
					}
					else throw new InvalidOperationException("Нет параметров границы");
					if (border2.isLine && border2.lineParams.HasValue)
					{
						var lineParams = border2.lineParams.GetValueOrDefault();
						if (IsEqualPoints(lineParams.p1, localClosest)) vec2 = new Vector3d(localClosest.X - lineParams.p2.X, localClosest.Y - lineParams.p2.Y, 0);
						else vec2 = new Vector3d(localClosest.X - lineParams.p1.X, localClosest.Y - lineParams.p1.Y, 0);
					}
					else if (border2.arcParams.HasValue)
					{
						var arcParams = border2.arcParams.GetValueOrDefault();
						var p1 = GetPoint3dFromPolar(arcParams.rad, arcParams.fi1);
						var p2 = GetPoint3dFromPolar(arcParams.rad, arcParams.fi2);
						Vector3d codirectional;
						vec2 = new Vector3d(arcParams.center.X - localClosest.X, arcParams.center.Y - localClosest.Y, 0).GetPerpendicularVector();
						if (IsEqualPoints(p1, localClosest)) codirectional = new Vector3d(localClosest.X - p2.X, localClosest.Y - p2.Y, 0);
						else codirectional = new Vector3d(localClosest.X - p1.X, localClosest.Y - p1.Y, 0);
						if (vec2.GetAngleTo(codirectional) > Math.PI / 2) vec2 = vec2.Negate();
					}
					else throw new InvalidOperationException("Нет параметров границы");
					normal = new Vector3d((vec1.X + vec2.X) / 2, (vec1.Y + vec2.Y) / 2, 0);
				}
			}
			catch(Autodesk.AutoCAD.Runtime.Exception ex)
            {
				ed.WriteMessage("{0}", ex);
            }

			return normal;
		}

		public static Vector3d GetArcNormal(Point3d pt, Point3d center, double rad, RegionProps reg)
        {
			if (reg.isCircle) return new Vector3d(pt.X - center.X, pt.Y - center.Y, 0);
			var arcsParams = reg.borders.FindAll(border => !border.isLine && border.arcParams.HasValue).Select(param => param.arcParams.GetValueOrDefault());
			var minRad = arcsParams.Min(param => param.rad);
			if (rad == minRad) return new Vector3d(center.X - pt.X, center.Y - pt.Y, 0);
			return new Vector3d(pt.X - center.X, pt.Y - center.Y, 0);
		}
		public static Point3d GetPoint3dFromPolar(double rad, double angle)
        {
			var cos = Math.Abs(Math.Cos(angle)) <= 1 ? Math.Cos(angle) : Math.Sign(Math.Cos(angle));
			var sin = Math.Abs(Math.Sin(angle)) <= 1 ? Math.Sin(angle) : Math.Sign(Math.Sin(angle));
			var X = rad * cos;
			var Y = rad * sin;
			return new Point3d(X, Y, 0);
        }

		private Point3dCollection findLineCircleIntersectPoint(Line l, Circle c)
		{
			var points = new Point3dCollection();
			l.IntersectWith(c, Intersect.OnBothOperands, points, new IntPtr(0), new IntPtr(0));
			if (points.Count != 0) return points;
			return null;
		}

		private double Distance(Point3d p1, Point3d p2)
		{
			return Math.Sqrt(Math.Pow((p1.X - p2.X), 2) + Math.Pow((p1.Y - p2.Y), 2));
		}

		private bool PointInLine(Point3d p, Point3d p1, Point3d p2)
		{
			return Math.Abs(Distance(p, p1) + Distance(p, p2) - Distance(p1, p2)) < EPS;
		}

		private bool CheckEqualLines(Line l1, Line l2)
		{
			var start1 = l1.StartPoint;
			var end1 = l1.EndPoint;
			var start2 = l2.StartPoint;
			var end2 = l2.EndPoint;
			var len1 = Distance(start1, end1);
			var len2 = Distance(start2, end2);
			if (len1 >= len2 && PointInLine(start2, start1, end1) && PointInLine(end2, start1, end1)) return true;
			if (len1 < len2 && PointInLine(start1, start2, end2) && PointInLine(end1, start2, end2)) return true;
			return false;
		}

		private bool CheckEqualArcs(Arc a1, Arc a2)
		{
			var start1 = a1.StartAngle;
			var end1 = Math.Abs(a1.EndAngle) < EPS ? a1.EndAngle + 2 * Math.PI : a1.EndAngle;
			var start2 = a2.StartAngle;
			var end2 = Math.Abs(a2.EndAngle) < EPS ? a2.EndAngle + 2 * Math.PI : a2.EndAngle;
			var center1 = a1.Center;
			var center2 = a2.Center;
			var rad1 = a1.Radius;
			var rad2 = a2.Radius;
			var len1 = (end1 - start1) * rad1;
			var len2 = (end2 - start2) * rad2;
			if (Math.Abs(rad1 - rad2) < EPS && IsEqualPoints(center1, center2))
			{
				if ((Math.Abs(len1 - len2) < EPS) && Math.Abs(start1 - start2) < EPS && Math.Abs(end1 - end2) < EPS) return true;
				if ((Math.Abs(len1 - len2) >= EPS) && len1 > len2 && start2 > start1 && start2 < end1 && end2 > start1 && end2 < end1) return true;
				if ((Math.Abs(len1 - len2) >= EPS) && len1 > len2 && Math.Abs(start2 - start1) < EPS && start2 < end1 && end2 > start1 && end2 < end1) return true;
				if ((Math.Abs(len1 - len2) >= EPS) && len1 > len2 && start2 > start1 && start2 < end1 && end2 > start1 && Math.Abs(end2 - end1) < EPS) return true;
				if ((Math.Abs(len1 - len2) >= EPS) && len1 < len2 && start1 > start2 && start1 < end2 && end1 > start2 && end1 < end2) return true;
				if ((Math.Abs(len1 - len2) >= EPS) && len1 < len2 && Math.Abs(start2 - start1) < EPS && start1 < end2 && end1 > start2 && end1 < end2) return true;
				if ((Math.Abs(len1 - len2) >= EPS) && len1 < len2 && start1 > start2 && start1 < end2 && end1 > start2 && Math.Abs(end2 - end1) < EPS) return true;

			}
			return false;
		}

		private double PtLineDistance(Point3d p, Line l)
		{
			var closest = l.GetClosestPointTo(p, false);
			return Distance(p, closest);
		}

		private double PtArcDistance(Point3d p, Arc a)
		{
			var closest = a.GetClosestPointTo(p, false);
			return Distance(p, closest);
		}

		private double PtEntityDistance(Point3d p, ObjectId objectId)
		{
			double dist = 0;
			try
			{
				var tr = db.TransactionManager.StartTransaction();
				using (tr)
				{
					var entity = tr.GetObject(objectId, OpenMode.ForRead) as Entity;
					if (entity is Line) return PtLineDistance(p, entity as Line);
					return PtArcDistance(p, entity as Arc);
				}
			}
			catch (Autodesk.AutoCAD.Runtime.Exception ex)
			{
				ed.WriteMessage("{0}", ex);
			}
			return dist;
		}

		private List<string> toFileHandle(bool withColor)
		{
			var file = new List<string> { };
			foreach (RegionProps props in REGION_MAP.Values)
			{
				string result = "";
				result += $"{props.num + 1 } ";
				if (withColor) {
					result += $"{props.ColorId} ";
				}
				result += $"{ props.area} {props.neighbors.Count} ";
				foreach (NeighborProps neighbor in props.neighbors)
				{
					result += $"{neighbor.num + 1} {neighbor.borderLenght} {neighbor.internalDist} {neighbor.externalDist} ";
				}
				file.Add(result);
			}
			return file;
		}

		private void writeToFile(List<string> lines)
		{
			var docPath =
		  Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

			using (var outputFile = new StreamWriter(Path.Combine(docPath, "PIKPARAMS.txt")))
			{
				foreach (string line in lines)
					outputFile.WriteLine(line);
			}
		}
	}
}

