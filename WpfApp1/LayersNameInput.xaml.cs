﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using AcAp = Autodesk.AutoCAD.ApplicationServices.Application;

namespace AutoCADPluginWpf
{
    /// <summary>
    /// Логика взаимодействия для LayersNameInput.xaml
    /// </summary>
    public partial class LayersNameInput : Window
    {
        LayersData _data;
        public LayersNameInput(LayersData data)
        {
            InitializeComponent();
            _data = data;
            this.DataContext = _data;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (_data.isValid)
            {
                this.DialogResult = true;
                this.Close();
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
