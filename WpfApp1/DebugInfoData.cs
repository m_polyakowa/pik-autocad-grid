﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;

namespace AutoCADPluginWpf
{
    public class DebugInfoData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string _debugTextSize;

        public string DebugLayerName { get; set; }

        public string DebugTextSize
        {
            get
            {
                return _debugTextSize;
            }
            set
            {
                _debugTextSize = value.Trim();

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsValid"));
                }
            }
        }

        public bool isValid
        {
            get
            {
                if (string.IsNullOrEmpty(_debugTextSize))
                {
                    return false;
                }
                try
                {
                    Convert.ToDouble(_debugTextSize);
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }

        }

    }
}
