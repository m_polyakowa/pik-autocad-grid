﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;

namespace AutoCADPluginWpf
{
    public partial class GeometryDataLayerNameInput : Window
    {
        GeometryDataLayerData _data;
        public GeometryDataLayerNameInput(GeometryDataLayerData data)
        {
            Database db = Autodesk.AutoCAD.ApplicationServices.Core.Application.DocumentManager.MdiActiveDocument.Database;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                InitializeComponent();
                _data = data;
                this.DataContext = _data;
                LayerTable layersTable = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForRead);
                int i = 0;
                foreach (ObjectId layerId in layersTable)
                {
                    LayerTableRecord layer = tr.GetObject(layerId, OpenMode.ForRead) as LayerTableRecord;
                    RadioButton btn = new RadioButton
                    {
                        Content = layer.Name,
                        IsChecked = layer.Name == _data.Name,
                    };

                    btn.Checked += new RoutedEventHandler(RadioButtonChange);

                    RadioBtnPanel.Children.Add(btn);
                    i++;
                }
            }
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void RadioButtonChange(object sender, RoutedEventArgs e)
        {
            RadioButton rb = sender as RadioButton;

            if (rb == null)
            {
                MessageBox.Show("Sender is not a RadioButton");
                return;
            }

            if (rb.IsChecked == true)
            {
                _data.Name = rb.Content as string;
            }
        }
    }
}
