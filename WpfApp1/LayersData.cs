﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;


namespace AutoCADPluginWpf
{
    public class LayersData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string _regionLayerName;
        string _textLayerName;
        public string RegionLayerName { 
            get
            {
                return _regionLayerName;
            }
            set
            {
                _regionLayerName = value.Trim();

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsValid"));
                }
            }
        }

        public string TextLayerName
        {
            get
            {
                return _textLayerName;
            }
            set
            {
                _textLayerName = value.Trim();

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsValid"));
                }
            }
        }

        public bool isValid
        {
            get
            {
                if (string.IsNullOrEmpty(RegionLayerName) || string.IsNullOrEmpty(TextLayerName))
                {
                    return false;
                }
                Database db = Application.DocumentManager.MdiActiveDocument.Database;
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    LayerTable layers = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForRead);
                    if (layers.Has(TextLayerName) || layers.Has(RegionLayerName))
                    {
                        return false;
                    }
                }
                  
                return true;
            }

        }

    }
}
