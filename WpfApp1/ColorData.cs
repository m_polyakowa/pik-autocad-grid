﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Colors;
using System.ComponentModel;

namespace AutoCADPluginWpf
{
    public class ColorData
    {
        //public event PropertyChangedEventHandler PropertyChanged;
        System.Windows.Media.Color _color;
        public System.Windows.Media.Color HatchColor
        {
            get { return _color; }
            set
            {
                _color = value;
                //PropertyChanged(this, new PropertyChangedEventArgs("HatchColor"));
            }
        }
    }
}
